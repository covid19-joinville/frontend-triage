import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

const axiosC = axios.create({
  baseURL: 'https://ebwvb8xo0c.execute-api.us-east-2.amazonaws.com/production'
})

Vue.use(VueAxios, axiosC)

export default (data) => {
  console.log('', data)
}