let currentInterceptor = null
export default (store) => {
    const axios = store._vm.axios
    store.subscribe((mutation, {cognito}) => {
        if (mutation.type !== 'cognito/setUser') return
        if (currentInterceptor)
            axios.interceptors.request.eject(currentInterceptor)
        if (cognito && cognito.session && cognito.session.idToken.jwtToken) {
            currentInterceptor = axios.interceptors.request.use((config) => {
                    const currentTokenInfo = store.getters['cognito/session']
                    const currentTS = new Date().getTime()
                    const expired = currentTokenInfo.idToken.payload.exp * 1000 < currentTS
                    if (!expired) {
                        config.headers['Authorization'] = `Bearer ${currentTokenInfo.idToken.jwtToken}`
                        return config;
                    } else {
                        return store.dispatch('cognito/fetchSession')
                            .then(session => {
                                config.headers['Authorization'] = `Bearer ${session.idToken.jwtToken}`
                                return config;
                            })
                    }
                },
                (error) => Promise.reject(error)
            )
        }
    })
}