import axiosStore from '@/plugins/axios-store'

const set = property => (store, payload) => {
    store[property] = payload
}

export const CognitoStore = {
    state: {
        email: '',
        isLoading: false,
        isReady: false,
        password: '',
        uuid: '',
        snackbar: {}
    },
    mutations: {
        setEmail: set('email'),
        setIsReady: set('isReady'),
        setIsLoading: set('isLoading'),
        setPassword: set('password'),
        setUser: set('user'),
        setUuid: set('uuid'),
        setSnackbar: set('snackbar')
    },

    plugins: [axiosStore],
}


