import Vue from 'vue'
import Vuex from 'vuex'
import attachCognitoModule from "@vuetify/vuex-cognito-module";
import { loginStore } from './login.js';
import { CognitoStore } from './cognito.js';

import axiosPlugin from '@/plugins/axios-store'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    loginStore,
    CognitoStore
  },
  plugins: [axiosPlugin]
})

attachCognitoModule(store, {
  userPoolId: process.env.VUE_APP_USER_POOL_ID || 'us-east-2_pxCbmFIhY',
  identityPoolId: process.env.VUE_APP_IDENTITY_POOL_ID || 'us-east-2:6cc6ad2e-1dff-43b9-848e-0a93c388efa0', // ID do grupo de identidades
  userPoolWebClientId: process.env.VUE_APP_CLIENT_ID || '38vfj1toe33lub2ur2ho66fjf1',
  region: process.env.VUE_APP_REGION || 'eu-east-2'
}, 'cognito')

store.dispatch('cognito/fetchSession')
    .finally(() => store.commit('setIsReady', true))



export default store