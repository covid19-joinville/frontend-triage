const set = property => (store, payload) => {
    store[property] = payload
}

export const loginStore = {
    state: {
        isCreated: false,
        pacient: {},
        prosseguir: false,
        cpf: ''
    },
    mutations: {
        setCreated: set('isCreated'),
        setPacient: set('pacient'),
        setProsseguir: set('prosseguir'),
        setCpf: set('cpf'),
    },
    actions: {
        setIsCreated({commit}, payload) {
            commit('setCreated', payload)
        },
        actionPacient({commit}, payload) {
            commit('setPacient', payload)
        },
        actionProsseguir({commit}, payload) {
            commit('setProsseguir', payload)
        },
        actionCpf({commit}, payload) {
            commit('setCpf', payload)
        }
    }
}

