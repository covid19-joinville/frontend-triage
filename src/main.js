import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import axios from './plugins/axios'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import './styles.css'
import router from './router'
import store from './store'
import VueTheMask from 'vue-the-mask'

import moment from "moment"
moment.locale('pt-BR');
Vue.use(VueTheMask)
Vue.config.productionTip = false

new Vue({
  store,
  router,
  vuetify,
  axios,
  render: h => h(App)
}).$mount('#app')
