import axios from 'axios'

let host = 'https://ebwvb8xo0c.execute-api.us-east-2.amazonaws.com/production'
let username = 'covid19'
let password = '$1$cFvoKJKl$85DlQ2WYyN9Hkga.Xg46h/'
if (process.env.NODE_ENV === 'development') {
  host = 'https://ebwvb8xo0c.execute-api.us-east-2.amazonaws.com/production'
}

function parseUrl (url) {
  let completeUrl = `${host}`
  if (url.match(/^\//) === null) {
    completeUrl = `${completeUrl}/`
  }

  return `${completeUrl}${url}`
}

function get (url, data) {
  return new Promise((resolve, reject) => {
    axios.get(parseUrl(url), { params: { data }, auth: { username, password } })
      .then(resolve)
      .catch(reject)
  })
}

function post (url, data) {
  return new Promise((resolve, reject) => {
    axios.post(parseUrl(url), data, { auth: { username, password } })
      .then(resolve)
      .catch(reject)
  })
}

export {
  get,
  post,
}
