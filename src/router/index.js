import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/about",
    name: "Sponsor",
    component: () => import("../views/Sponsors.vue")
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Login.vue")
  },
  {
    path: "/complete-profile",
    name: "completar-cadastro",
    component: () => import("../views/CompleteProfile.vue")
  },
  {
    path: '/team-login',
    name: 'Login Equipe Médica',
    component: () => import('../views/Team-Login.vue')
  },
  {
    path: '/signup',
    name: 'Registrar-se',
    component: () => import('../views/Signup.vue')
  },
  {
    path: '/change-password',
    name: 'Alterar senha',
    component: () => import('../views/Change-Password.vue')
  },
  {
    path: '/symptoms',
    name: 'Sintomas',
    component: () => import('../views/Symptoms.vue')
  },
  {
    path: "/purpose",
    name: "objetivo",
    component: () => import("../views/Purpose.vue")
  },
  {
    path: "/symptoms",
    name: "Sintomas",
    component: () => import("../views/Symptoms.vue")
  },
  {
    path: "/patient",
    name: "Paciente",
    component: () => import("../views/Patient.vue")
  },
  {
    path: "/list_symptoms",
    name: "Exames",
    component: () => import("../views/Events.vue")
  },
  {
    path: "/exames",
    name: "Exames",
    component: () => import("../views/Exames.vue")
  },
  {
    path: "/contact-phones",
    name: "Contact",
    component: () => import("../views/Contact.vue")
  },
  {
    path: "/locations",
    name: "Locations",
    component: () => import("../views/Locations.vue")
  },
  {
    path: "/profile",
    name: "Profile",
    component: () => import("../views/Profile.vue")
  },
  {
    path: "/terms",
    name: "Terms",
    component: () => import("../views/MoreTerm.vue")
  }
];

const router = new VueRouter({
  routes
});

export default router;
