module.exports = {
    fc: [
        {text: "< 100 bmp", id: 1},
        {text: "> 100 bmp", id: 2},
    ],
    pas: [
        {text: "> 100 mmHg", id: 1},
        {text: "< 90 mmHg", id: 2},
        {text: "> 90 mmHg e < 100 mmHg", id: 3},

    ],
    sao2: [
        {text: ">= 95%", id: 1},
        {text: "< 95%", id: 2},

    ],
    dispineia: [
        {text: "Respiração difícil", id: 1},
        {text: "Respirando normalmente", id: 2},
    ],
    febre: [
        {text: "< 35", id: 1},
        {text: ">= 35 e < 38", id: 2},
        {text: ">= 38 e < 39", id: 3},
        {text: ">= 39", id: 4}
    ]
}