module.exports = {
    nome: null,
    dataNascimento: null,
    nomeDaMae: null,
    cpf: null,
    cep: null,
    endereco: null,
    telefone: null,
    operadora: 'S.U.S',
    localizacao: {
        longitude: "0",
        latitude: "0"
    },
    ENUM_PESSOA : "PACIENTE"
}