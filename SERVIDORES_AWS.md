{
    'us-east-1': 'Z3AQBSTGFYJSTF',
    'us-west-2' : 'Z3BJ6K6RIION7M',
    'us-west-1' : 'Z2F56UZL2M1ACD',
    'eu-west-1' : 'Z1BKCTXD74EZPE',
    'ap-southeast-1' : 'Z3O0J2DXBE1FTB',
    'ap-southeast-2' : 'Z1WCIGYICN2BYD',
    'ap-northeast-1' : 'Z2M4EHUR26P7ZW',
    'sa-east-1' : 'Z7KQH4QJS55SO',
    'us-gov-west-1' : 'Z31GFT0UA1I2HV'
}

{
    "Version":"2012-10-17",
    "Statement":[{
      "Sid":"PublicReadGetObject",
          "Effect":"Allow",
        "Principal": "*",
        "Action":["s3:GetObject"],
        "Resource":["arn:aws:s3:::example-bucket/*"
        ]
      }
    ]
  }
  https://docs.aws.amazon.com/sdk-for-ruby/v1/api/AWS/Route53/HostedZone.html