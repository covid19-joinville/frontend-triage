#!/bin/bash
npm install
npm run deploy
pip install awscli
aws s3 cp ./ s3://$S3_BUCKET_NAME/ --recursive --exclude "*" --include "dist/*"